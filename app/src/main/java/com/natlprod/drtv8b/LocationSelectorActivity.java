package com.natlprod.drtv8b;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

/**
 * Created by john on 8/1/2016.
 */
public class LocationSelectorActivity extends Activity implements AdapterView.OnItemSelectedListener {
    ArrayList<ArrayList<String>> venues = new ArrayList<>();


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selector);

        Log.i("spinner","begin locationselector" );

        String colors[] = {"Red","Blue","White","Yellow","Black", "Green","Purple","Orange","Grey"};

        ArrayList<String> venue1 = new ArrayList<String>();
        ArrayList<String> venue2 = new ArrayList<String>();
        ArrayList<String> venue3 = new ArrayList<String>();

        venue1.add("venue1ID");
        venue1.add("venue1Name");
        venue1.add("http://myconstantcare.com/php_tests/drtv/demo.html");

        venue2.add("venue2ID");
        venue2.add("venue2Name");
        venue2.add("http://myconstantcare.com/php_tests/drtv/demo.html");

        venue3.add("venue3ID");
        venue3.add("venue3Name");
        venue3.add("http://myconstantcare.com/php_tests/drtv/demo.html");


//        String venue2[] = {"venue2ID","venue2Name","http://myconstantcare.com/php_tests/drtv/demo.html"};
//        String venue3[] = {"venue3ID","venue3Name","http://myconstantcare.com/php_tests/drtv/demo.html"};

       venues.add(venue1);
        venues.add(venue2);
        venues.add(venue3);

        ArrayList<String> locationNames = new ArrayList<String>();

        locationNames.add("Choose location");
        locationNames.add(venue1.get(1));
        locationNames.add(venue2.get(1));
        locationNames.add(venue3.get(1));

        Spinner spinner = (Spinner) findViewById(R.id.locationSpinner);
        spinner.setOnItemSelectedListener(this);

//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.planets_array, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, locationNames);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);


    }

    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        // your code here

        Log.i("spinner","position = " + Integer.toString(position) );

        int venueIndex = position;
        String venueUrl = venues.get(position).get(2);

        if(venueIndex > 0) {
            venueIndex = venueIndex -1;
            Intent intent = new Intent(this, MyMainActivity.class);
            intent.putExtra("venueIndex", venueIndex);
            intent.putExtra("venueUrl", venueUrl);
        startActivity(intent);
        }
//        Log.i("spinner","id = " + Integer.toString(position) );


    }
    public void onNothingSelected(AdapterView<?> parentView) {
        // your code here
    }
}