package com.natlprod.drtv8b;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import android.widget.ViewFlipper;

/**
 * Created by john on 2/22/2016.
 */
public class SelfContainedActivity extends AppCompatActivity  implements View.OnClickListener, View.OnTouchListener {
    //video starts maximized(0), 1 when minimized
    public int videoState = 0;


    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private ViewFlipper mViewFlipper;
    private Animation.AnimationListener mAnimationListener;
    private Context mContext;
    int FCount1 = 0;
    int FCount2 = 0;
    int FCount3 = 0;

    int numberOfPages = 2;
    protected void onCreate(Bundle savedInstanceState) {


//        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        super.onCreate(savedInstanceState);


//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
//        decorView.setSystemUiVisibility(uiOptions);


        //getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);



//        View decorView = getWindow().getDecorView();
//        decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);



//        if(Build.VERSION.SDK_INT < 19){ //19 or above api
//            View v = this.getWindow().getDecorView();
//        v.setSystemUiVisibility(View.GONE);
//    } else {
//        //for lower api versions.
//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.Systems;
//        decorView.setSystemUiVisibility(uiOptions);
//    }



        getWindow().getDecorView().setSystemUiVisibility(View.GONE);

        setContentView(R.layout.activity_self_contained);

        VideoView videoview = (VideoView) findViewById(R.id.video_view);

//        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.ohio_state);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.shortv);

//        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.gen1);


        videoview.setVideoURI(uri);

        videoview.setOnClickListener(this);
        videoview.setOnTouchListener(this);



        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        videoview.start();

        ImageButton banner = (ImageButton)findViewById(R.id.banner);
        //banner.setOnClickListener(this);

//        ImageButton ss = (ImageButton) findViewById(R.id.ss_eagle);
//        ss.setOnClickListener(this);

        ImageButton eagle = (ImageButton) findViewById(R.id.ss_eagle);
        eagle.setOnClickListener(this);

        ImageButton gnc = (ImageButton) findViewById(R.id.ss_gnc);
        gnc.setOnClickListener(this);

        ImageButton izzazu = (ImageButton) findViewById(R.id.ss_izzazu);
        izzazu.setOnClickListener(this);

        ImageButton weather = (ImageButton) findViewById(R.id.weather);
        weather.setOnClickListener(this);

        ImageButton learn = (ImageButton) findViewById(R.id.learn);
        learn.setOnClickListener(this);

        ImageButton free = (ImageButton) findViewById(R.id.free);
        free.setOnClickListener(this);

        ImageButton feedback = (ImageButton) findViewById(R.id.feedback);
        feedback.setOnClickListener(this);

        ImageButton favorites = (ImageButton) findViewById(R.id.favorites);
        favorites.setOnClickListener(this);

        ImageButton checkIn = (ImageButton) findViewById(R.id.check_in);
        checkIn.setOnClickListener(this);

        ImageButton patientSurvey = (ImageButton) findViewById(R.id.patient_survey);
        patientSurvey.setOnClickListener(this);

        ImageButton about = (ImageButton) findViewById(R.id.about);
        about.setOnClickListener(this);

        ImageButton share = (ImageButton) findViewById(R.id.share);
        share.setOnClickListener(this);



        ImageButton tellUsMore = (ImageButton) findViewById(R.id.return_to_video);
        tellUsMore.setOnClickListener(this);

        ImageButton fakebt1 = (ImageButton) findViewById(R.id.fakeButton1);
        fakebt1.setOnClickListener(this);

        ImageButton fakeSub1 = (ImageButton) findViewById(R.id.fakeSub1);
        fakeSub1.setOnClickListener(this);

        ImageButton fakebt2 = (ImageButton) findViewById(R.id.fakeButton2);
        fakebt2.setOnClickListener(this);

        ImageButton fakeSub2 = (ImageButton) findViewById(R.id.fakeSub2);
        fakeSub2.setOnClickListener(this);

        ImageButton fakebt3 = (ImageButton) findViewById(R.id.fakeButton3);
        fakebt3.setOnClickListener(this);

        ImageButton fakeSub3 = (ImageButton) findViewById(R.id.fakeSub3);
        fakeSub3.setOnClickListener(this);


        mContext = this;
        mViewFlipper = (ViewFlipper) this.findViewById(R.id.view_flipperz);

        mViewFlipper.setAutoStart(true);
        mViewFlipper.setFlipInterval(15000);
        mViewFlipper.startFlipping();


       // ImageButton center = new ImageButton(this);
        //center.setImageResource(R.drawable.weather1);

        // minVideo();

    }

    private void changeState(){
        if (videoState == 0){
            videoState=1;
        }
        else{
            videoState=0;
        }
    }
    private void minVideo(){
        Log.i("minVideo", "e");
        //videoState=1;
        VideoView videoview = (VideoView) findViewById(R.id.video_view);

        RelativeLayout.LayoutParams videoviewlp = new RelativeLayout.LayoutParams(301, 170);
//        RelativeLayout.LayoutParams videoviewlp = new RelativeLayout.LayoutParams(395, 300);

        //videoviewlp.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
       // videoviewlp.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);


        //RelativeLayout.LayoutParams videoviewlp = (RelativeLayout.LayoutParams)videoview.getLayoutParams();

       //videoviewlp.width=170;
        //videoviewlp.height=170;


        videoviewlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        videoviewlp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        //videoviewlp.addRule(RelativeLayout.ALIGN_BOTTOM);
        videoview.setLayoutParams(videoviewlp);
        videoview.invalidate();
        changeState();
    }

    public void maxVideo(){
        Log.i("maxVideo","e"  );
        //videoState=0;
        VideoView videoview = (VideoView) findViewById(R.id.video_view);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)videoview.getLayoutParams();
       // RelativeLayout.LayoutParams videoviewlp = new RelativeLayout.LayoutParams;

        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        //params.addRule(RelativeLayout.Hei, 0);



        params.addRule(RelativeLayout.ALIGN_TOP, R.id.about);
        params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.view_flipperz);

        params.addRule(RelativeLayout.RIGHT_OF,R.id.about);
        params.addRule(RelativeLayout.LEFT_OF, R.id.view_flipperz);

        videoview.setLayoutParams(params);
        changeState();
    }
    public boolean onTouch(View v,MotionEvent m){
        //minVideo();
        Log.i("event","touch"  );
        ImageButton fakebt1 = (ImageButton)findViewById(R.id.fakeButton1);
        fakebt1.setVisibility(View.INVISIBLE);

        ImageButton fakeSub1 = (ImageButton)findViewById(R.id.fakeSub1);
        fakeSub1.setVisibility(View.INVISIBLE);

        ImageButton fakebt2 = (ImageButton)findViewById(R.id.fakeButton2);
        fakebt2.setVisibility(View.INVISIBLE);

        ImageButton fakeSub2 = (ImageButton)findViewById(R.id.fakeSub2);
        fakeSub2.setVisibility(View.INVISIBLE);
        ImageButton fakebt3 = (ImageButton)findViewById(R.id.fakeButton3);
        fakebt3.setVisibility(View.INVISIBLE);

        ImageButton fakeSub3 = (ImageButton)findViewById(R.id.fakeSub3);
        fakeSub3.setVisibility(View.INVISIBLE);




//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);


        if(m.getAction() == MotionEvent.ACTION_DOWN) {

         if (videoState == 1) {
                Log.i("max", "vid");

                maxVideo();
            }
        }
        //changeState();
        return true;
    }
    public void onClick(View v) {



        ImageButton fakebt1 = (ImageButton)findViewById(R.id.fakeButton1);
        fakebt1.setVisibility(View.INVISIBLE);

        ImageButton fakeSub1 = (ImageButton)findViewById(R.id.fakeSub1);
        fakeSub1.setVisibility(View.INVISIBLE);

        ImageButton fakebt2 = (ImageButton)findViewById(R.id.fakeButton2);
        fakebt2.setVisibility(View.INVISIBLE);

        ImageButton fakeSub2 = (ImageButton)findViewById(R.id.fakeSub2);
        fakeSub2.setVisibility(View.INVISIBLE);

        ImageButton fakebt3 = (ImageButton)findViewById(R.id.fakeButton3);
        fakebt3.setVisibility(View.INVISIBLE);

        ImageButton fakeSub3 = (ImageButton)findViewById(R.id.fakeSub3);
        fakeSub3.setVisibility(View.INVISIBLE);


//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);

        switch (v.getId()) {

            case R.id.banner:
                Log.i("banner clicked", "clicked");
                ImageView center = (ImageView)findViewById(R.id.center);
                center.setImageResource(R.drawable.center);
                if(videoState ==0){
                    minVideo();
                }
                break;

            case R.id.ss_eagle:
                Log.i("ss clicked", "clicked");

                ImageView ssCenter = (ImageView)findViewById(R.id.center);
                ssCenter.setImageResource(R.drawable.center_eagle);
                if(videoState ==0){
                    minVideo();

                }            break;

            case R.id.about:
                Log.i("about", "clicked");

                ImageView about = (ImageView)findViewById(R.id.center);
                about.setImageResource(R.drawable.center_about);
                if(videoState ==0){
                    minVideo();

                }
                break;
            case R.id.learn:
                Log.i("learn clicked","clicked"  );

                ImageView learnCenter = (ImageView)findViewById(R.id.center);
                learnCenter.setImageResource(R.drawable.center_learn);
                if(videoState ==0){
                    minVideo();

                }

            break;

            case R.id.free:
                Log.i("banner clicked", "clicked");
                ImageView free = (ImageView)findViewById(R.id.center);
                free.setImageResource(R.drawable.center_coupons);
                if(videoState ==0){
                    minVideo();
                }
                break;

            case R.id.feedback:
                Log.i("banner clicked", "clicked");
                ;

                ImageView feedback = (ImageView)findViewById(R.id.center);



                if(FCount3 == 0){
                    fakebt3.setVisibility(View.VISIBLE);
                    feedback.setImageResource(R.drawable.center_feedback);
                }
                else if (FCount3 == 1){
                    fakeSub3.setVisibility(View.VISIBLE);
                    feedback.setImageResource(R.drawable.center_feedback2);
                }
                else{

                    fakebt3.setVisibility(View.VISIBLE);
                    feedback.setImageResource(R.drawable.center_feedback);
                }


                if(videoState ==0){
                    minVideo();
                }
                break;


            case R.id.favorites:
                Log.i("banner clicked", "clicked");
                ImageView faveorites = (ImageView)findViewById(R.id.center);
                faveorites.setImageResource(R.drawable.center_favorites);
                if(videoState ==0){
                    minVideo();
                }
                break;


            case R.id.check_in:
                Log.i("banner clicked", "clicked");
                ImageView checkIn = (ImageView)findViewById(R.id.center);
                checkIn.setImageResource(R.drawable.center_check_in);
                if(videoState ==0){
                    minVideo();
                }
                break;


            case R.id.patient_survey:
                Log.i("banner clicked", "clicked");
                ImageView patientSurvey = (ImageView)findViewById(R.id.center);


//                ImageButton fakebt1 = (ImageButton)findViewById(R.id.fakeButton1);
                if(FCount1 == 0){
                    fakebt1.setVisibility(View.VISIBLE);
                    patientSurvey.setImageResource(R.drawable.center_patient_survey);
                }
                else if (FCount1 == 1){
                    fakeSub1.setVisibility(View.VISIBLE);
                    patientSurvey.setImageResource(R.drawable.center_patient_survey2);
                }
                else{

                    fakebt1.setVisibility(View.VISIBLE);
                    patientSurvey.setImageResource(R.drawable.center_patient_survey);
                }
                if(videoState ==0){
                    minVideo();
                }
                break;





            case R.id.share:
                Log.i("new", "start share");
                Log.i("new", "FCount2 =  " + Integer.toString(FCount2));
                ImageView share = (ImageView)findViewById(R.id.center);


                if(FCount2 == 0){
                    fakebt2.setVisibility(View.VISIBLE);
                    share.setImageResource(R.drawable.center_patient_education);
                }
                else if (FCount2 == 1){
                    fakeSub2.setVisibility(View.VISIBLE);
                    share.setImageResource(R.drawable.center_patient_education_page2);
                }
                else{

                    fakebt2.setVisibility(View.VISIBLE);
                    share.setImageResource(R.drawable.center_patient_education);
                }




                if(videoState ==0){
                    minVideo();
                }
                break;


            case R.id.return_to_video:
                Log.i("banner clicked", "clicked");

                if(videoState ==1){
                    maxVideo();
                }
                break;

            case R.id.weather:
                Log.i("banner clicked", "clicked");
                ImageView weather = (ImageView)findViewById(R.id.center);
                weather.setImageResource(R.drawable.center_weather);
                if(videoState ==0){
                    minVideo();
                }
                break;


            case R.id.ss_gnc:
                Log.i("banner clicked", "clicked");
                ImageView ss_gnc = (ImageView)findViewById(R.id.center);
                ss_gnc.setImageResource(R.drawable.center_gnc);
                if(videoState ==0){
                    minVideo();
                }
                break;

            case R.id.ss_izzazu:
                Log.i("banner clicked", "clicked");
                ImageView ss_izzazu = (ImageView)findViewById(R.id.center);
                ss_izzazu.setImageResource(R.drawable.center_izzazu);
                if(videoState ==0){
                    minVideo();
                }
                break;

            case R.id.fakeButton1:
                Log.i("new", "ps next in");
//                Log.i("banner clicked", "clicked");
                if(FCount1<numberOfPages){
                    ImageView centerImg = (ImageView)findViewById(R.id.center);
                    if(FCount1 == 0){
                        centerImg.setImageResource(R.drawable.center_patient_survey2);
                        fakeSub1.setVisibility(View.VISIBLE);
                    }
                    FCount1 +=1;

                    Log.i("new", "ps next out... FCount1 =  " + Integer.toString(FCount1));
                }

                break;

            case R.id.fakeSub1:
//                Log.i("banner clicked", "clicked");
                    ImageView centerImg = (ImageView)findViewById(R.id.center);
                        centerImg.setImageResource(R.drawable.center_patient_survey_c);
//                        fakebt1.setVisibility(View.VISIBLE);
                        FCount1=0;
                break;
            case R.id.fakeButton2:
                Log.i("new", "pe next in");
                if(FCount2<numberOfPages){
                    ImageView centerImg2 = (ImageView)findViewById(R.id.center);
                    if(FCount2 == 0){
                        centerImg2.setImageResource(R.drawable.center_patient_education_page2);
                        fakeSub2.setVisibility(View.VISIBLE);
                    }
                    FCount2 +=1;

                Log.i("new", "pe next out... FCount2 =  " + Integer.toString(FCount2));

            }
                break;
            case R.id.fakeSub2:
//                Log.i("banner clicked", "clicked");
                ImageView centerImg2 = (ImageView)findViewById(R.id.center);
                centerImg2.setImageResource(R.drawable.center_patient_ed_c);
//                        fakebt1.setVisibility(View.VISIBLE);
                FCount2=0;
                break;
            case R.id.fakeButton3:
                Log.i("new", "fb next in");
//                Log.i("banner clicked", "clicked");
                if(FCount3<numberOfPages){
                    ImageView centerImg3 = (ImageView)findViewById(R.id.center);
                    if(FCount3 == 0){
                        centerImg3.setImageResource(R.drawable.center_feedback2);
                        fakeSub3.setVisibility(View.VISIBLE);
                    }
                    FCount3 +=1;

                    Log.i("new", "fb next out... FCount3 =  " + Integer.toString(FCount3));
                }
                break;
            case R.id.fakeSub3:
//                Log.i("banner clicked", "clicked");
                ImageView centerImg3 = (ImageView)findViewById(R.id.center);
                centerImg3.setImageResource(R.drawable.center_feedback_c);
//                        fakebt1.setVisibility(View.VISIBLE);
                FCount3=0;
                break;

        }
    }

}
