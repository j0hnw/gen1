package com.natlprod.drtv8b;
//package com.natlprod.drtv8;
//
import org.json.JSONException;
import org.json.JSONObject;


//import com.compdigitec.libvlcandroidsample.R;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends Activity implements View.OnClickListener {
    public final static String TAG = "VideoActivity";
    //Adsparx components.
    String deviceId;
    String plotterId;
    int nextReq;

	int videoState = 0;

	VideoFragment newFrag1 ;

	VideoFragment2 newFrag2 ;

    public final static String LOCATION = "com.compdigitec.libvlcandroidsample.VideoActivity.location";






    @Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate");

        setContentView(R.layout.layout);
		Log.i("php","pre serverCom " );

//		String phpResults =  MyMainActivity.serverComm("http://myconstantcare.com/android_connect/android_post_test.php",false);

//		Log.i("php","phpResults" + phpResults );


//		String comtest2Results = MyMainActivity.connTest2();
//		Log.i("php","phpResults" + comtest2Results );

		Log.i("php","pre contest2" );

		new conTest2().execute();

		Log.i("php","pre contest3" );

		new conTest3().execute();



		WebView bannerWV;
		WebView cornerWV;
		WebView viewFlipperWV;
		WebView topButtonsWV;
		WebView leftButtonsWV;

		bannerWV = (WebView) findViewById(R.id.banner);
		bannerWV.getSettings().setJavaScriptEnabled(true);
		bannerWV.loadUrl("http://myconstantcare.com/php_tests/drtv/banner.html");

		cornerWV = (WebView) findViewById(R.id.corner);
		cornerWV.getSettings().setJavaScriptEnabled(true);
		cornerWV.loadUrl("http://myconstantcare.com/php_tests/drtv/corner.html");
//
		viewFlipperWV = (WebView) findViewById(R.id.view_flipperz);
		viewFlipperWV.getSettings().setJavaScriptEnabled(true);
		viewFlipperWV.loadUrl("http://myconstantcare.com/php_tests/drtv/sky.html");

		topButtonsWV = (WebView) findViewById(R.id.topButtons);
		topButtonsWV.getSettings().setJavaScriptEnabled(true);
		topButtonsWV.loadUrl("http://myconstantcare.com/php_tests/drtv/topbuttons.html");
//		topButtonsWV.loadUrl("http://myconstantcare.com/php_tests/drtv/corner.html");

		leftButtonsWV = (WebView) findViewById(R.id.leftButtons);
		leftButtonsWV.getSettings().setJavaScriptEnabled(true);
		leftButtonsWV.loadUrl("http://myconstantcare.com/php_tests/drtv/leftbuttons.html");



		Button cornerButton;
//		Button aboutButton;
		Button aboutButton;
		Button checkInButton;
		Button patientSurvey;
		Button patientEd;
		Button weather;
		Button returnToVideo;

		cornerButton = (Button)findViewById(R.id.cornerButton);
		cornerButton.setOnClickListener(this);

//		aboutButton = (Button)findViewById(R.id.about);
//		aboutButton.setOnClickListener(this);


		aboutButton = (Button)findViewById(R.id.about);
		aboutButton.setOnClickListener(this);

		checkInButton = (Button)findViewById(R.id.check_in);
		checkInButton.setOnClickListener(this);

		patientSurvey = (Button)findViewById(R.id.patient_survey);
		patientSurvey.setOnClickListener(this);

		patientEd = (Button)findViewById(R.id.patient_ed);
		patientEd.setOnClickListener(this);

		weather = (Button)findViewById(R.id.weather);
		weather.setOnClickListener(this);

		returnToVideo = (Button)findViewById(R.id.return_to_video);
		returnToVideo.setOnClickListener(this);

		Button learn;
		Button free;
		Button feedback;
		Button favorites;
		Button sky;

		learn = (Button)findViewById(R.id.learn);
		learn.setOnClickListener(this);

		free = (Button)findViewById(R.id.free);
		free.setOnClickListener(this);

		feedback = (Button)findViewById(R.id.feedback);
		feedback.setOnClickListener(this);

		favorites = (Button)findViewById(R.id.favorites);
		favorites.setOnClickListener(this);

		sky = (Button)findViewById(R.id.sky);
		sky.setOnClickListener(this);


		FrameLayout vidCont1;
		FrameLayout vidCont2;


		vidCont1 = (FrameLayout)findViewById(R.id.video_container1);
		vidCont1.setOnClickListener(this);

		vidCont2 = (FrameLayout)findViewById(R.id.video_container2);
		vidCont2.setOnClickListener(this);

//		setContentView(com.natlprod.drtv8.layout.layout);



		reportDeviceInformation();

        String video = "rtp://@224.0.0.2:10000";


//		VideoFragment newFrag1 = VideoFragment.newInstance(video);
//
//		VideoFragment newFrag2 = VideoFragment.newInstance(video);

		newFrag1 = VideoFragment.newInstance(video);

		newFrag2 = VideoFragment2.newInstance(video);

//		newFrag1.setSize(200,200);

//		this.showFragment(
//                VideoFragment.newInstance(video),
//                R.id.video_container1);
//        this.showFragment(VideoInfoFragment.newInstance(""),
//        		R.id.video_container);


//		this.show2Fragment(
//				newFrag1,
//				R.id.video_container1,
//				newFrag2,
//				R.id.video_container2	);

//		this.showFragment(
//				newFrag2,
//				R.id.video_container2);

		this.showFragment(
				newFrag1,
				R.id.video_container1);
//		this.showFragment(VideoInfoFragment.newInstance(""),
//				R.id.video_container);
//



//		this.show2Fragment(
//				newFrag1,
//				R.id.video_container1,
//				newFrag2,
//				R.id.video_container2	);


//		destroyItem(newFrag1);
//
//		this.showFragment(
//				newFrag2,
//				R.id.video_container2);

//		this.showFragment(
//				newFrag1,
//				R.id.video_container1);

        startAdsparx();
        /*Configuration currentConfiguration = getResources().getConfiguration();
        if(currentConfiguration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        	VideoInfoFragment(View.GONE);
        }*/

    }





	public void destroyItem(Object object) {

		FragmentManager manager = ((Fragment) object).getFragmentManager();
		FragmentTransaction trans = manager.beginTransaction();
		trans.remove((Fragment) object);
		trans.commit();
	}


    protected void showFragment(Fragment newFragment, int container) {
        FragmentTransaction transaction = this.getFragmentManager()
                .beginTransaction();
        transaction.add(container, newFragment);
        transaction.commit();
    }

	protected void show2Fragment(Fragment newFragment1, int container1, Fragment newFragment2, int container2) {
		FragmentTransaction transaction = this.getFragmentManager()
				.beginTransaction();
		transaction.add(container2, newFragment2);
		transaction.add(container1, newFragment1);


		transaction.commit();
	}


    /*
     * Hide Video elements if you want
     * (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
        	VideoInfoFragment(View.VISIBLE);

        } else {
        	VideoInfoFragment(View.INVISIBLE);

        }
    }



	private void VideoInfoFragment(int visible) {
		// TODO Auto-generated method stub
    	Log.d(TAG, "removing the second fragmet");
    	FrameLayout listFragmentContainer = (FrameLayout) findViewById(R.id.video_container);
        listFragmentContainer.setVisibility(visible);

	}


	@Override
    public void onResume(){
    	super.onResume();

		Log.d(TAG, "onResume");
    }


    @Override
    public void onPause(){
    	super.onPause();
		Log.d(TAG, "onPause");
    }

    @Override
    public void onDestroy(){
    	super.onDestroy();
		Log.d(TAG, "onDestroy");
    }


	public void startAdsparx() {
		WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = manager.getConnectionInfo();
		deviceId = info.getMacAddress();
		deviceId = deviceId.replace(":", "-");
		Log.i(TAG, "device id"+deviceId);
		String plotterID = getPlotterID(deviceId);
		if (plotterID != null) {
			FrameLayout listFragmentContainer = (FrameLayout) findViewById(R.id.video_container1);
			listFragmentContainer.setOnTouchListener(new OnTouchListener() {
				@SuppressLint("ClickableViewAccessibility")
				@Override
				public boolean onTouch(View view, MotionEvent event) {
					Log.i(TAG, "Adsparx OnTouch:" + event.toString());
						if (event.getAction() != MotionEvent.ACTION_DOWN) {
							return false;
						}
						return doClick();
					}
				});
		nextReq = 0;
		reportServer(deviceId, plotterID, "view",nextReq);
		}else {
			Toast.makeText(getBaseContext(), "connection to plotter failed.",
					Toast.LENGTH_LONG).show();
		}
	}



	private boolean doClick() {

		String urlToOpen = hitMetaURl();
		if(urlToOpen != null){
			hitClickReport();
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(urlToOpen));
			startActivity(i);
		}
		return true;
	}


	private void hitClickReport() {

		reportServer(deviceId, plotterId, "click", 0);

	}

	private String hitMetaURl() {

		String metaURL = null;
		String metaReqUrl = "http://192.168.10.106:8080/getmetaURL/"+deviceId+"/"+plotterId;
		String metaURLResp = AdSparx.serverComm(metaReqUrl,true);
		if (metaURLResp !=null){
			try {
				metaURL = metaURLResp.replaceAll("\\\\", "");
				JSONObject url = new JSONObject(metaURL);
				metaURL = url.getString("metaUrl");
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			Toast.makeText(getBaseContext(), "Error occured during MetaURL..!!",
					Toast.LENGTH_LONG).show();
		}
		return metaURL;

	}
	private String getPlotterID(String deviceId) {

		String url = "http://cms.gen1mediagroup.com/manage/api/getPlotterId/"+deviceId+"/";
    	//String url = "http://107.170.71.238/manage/api/getPlotterId/12-34-56-78-90/";
		String resp = AdSparx.serverComm(url, true);

		Log.i(TAG, "Res URL: " + resp);

    	try {
    		String myJsonString = resp.replaceAll("\\\\","");
    		JSONObject job = new JSONObject(myJsonString);
			plotterId =job.getJSONObject("message").getString("PlotterID");
			Log.i(TAG, "mediaID: " + plotterId);

		  } catch(JSONException e) {
			  Log.e(TAG, e.toString());
		  }
    	return plotterId;

	}
	/*
	 * {"watchedVideoFileName":"","watchedVideoProgress":0,"plotterId":"AVDSFJKJHdsjh4372", "deviceId":"dc-74-fd-43-sd-34-54","deviceUA":"","deviceIP":"","event":"view"}
	 * */

	private void reportServer(final String deviceId2, final String plotterID,final String event,final int time) {
		final String reportingserver = "http://192.168.10.106:8080/deviceinfo";
		String userAgent = System.getProperty("http.agent");
		final JSONObject report = new JSONObject();
		try {
			report.put("watchedVideoFileName", "");
			report.put("watchedVideoProgress", "");
			report.put("plotterId", plotterID);
			report.put("macId", deviceId2);
			report.put("deviceUA",userAgent );
			report.put("deviceIP", "");
			report.put("event", event);
			Log.i(TAG, "Res URL: " + report);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (event == "click"){
			AdSparx.reportingServer(reportingserver ,report);
		}else {
			eventReport(reportingserver,report,time);
		}

	}

	private void eventReport(final String reportingserver, final JSONObject report, final int time) {
		new android.os.Handler().postDelayed(
			    new Runnable() {
			        public void run() {
			            Log.i("tag", "This'll run "+time+"milliseconds later");
			            String nextReqToHit = AdSparx.reportingServer(reportingserver ,report);
			            if (nextReqToHit !=null ){
			            	try {
								JSONObject hitInterval = new JSONObject(nextReqToHit);
								nextReq = hitInterval.getInt("hitAfter");
								Log.i(TAG, "What is the next request interval"+nextReq);
								if (nextReq >0){
									eventReport(reportingserver, report, nextReq);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
				        }else {
				        	Toast.makeText(getBaseContext(), "reporting failed",
									Toast.LENGTH_LONG).show();
				        }
			        }
				},
		time);

	}


	private void reportDeviceInformation(){
		Log.i("DevInfo","enter RDI ");

		IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		Intent batteryStatus = this.registerReceiver(null, ifilter);


		// Are we charging / charged?
		int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
		boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
				status == BatteryManager.BATTERY_STATUS_FULL;

// How are we charging?
		int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
		boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;


// battery level
		int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
		int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

		float batteryPct = (level / (float)scale) *100;


// wifi level
		WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
		int numberOfLevels = 100;
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int wifiLevel = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);


		String ssid = wifiInfo.getSSID();
		String macAddress = wifiInfo.getMacAddress();

// time


		Calendar c = Calendar.getInstance();
		int hour = c.get(Calendar.HOUR_OF_DAY);
		int minute = c.get(Calendar.MINUTE);
		int seconds = c.get(Calendar.SECOND);
		TimeZone timeZone = TimeZone.getDefault();
		String timeZoneString = timeZone.getDisplayName(false, TimeZone.SHORT);


//log changing status
		Log.i("DevInfo","Charging = "+isCharging);
		Log.i("DevInfo","usbCharge = "+usbCharge);
		Log.i("DevInfo","acCharge = "+acCharge);
		Log.i("DevInfo","batteryPct = "+ batteryPct + "%");
		Log.i("DevInfo","wifiLevel = "+ wifiLevel + "%");
		Log.i("DevInfo","ssid = "+ ssid);
		Log.i("DevInfo","MAC Address = "+ macAddress);



		Log.i("DevInfo","time = "+ hour + ":"+ minute + ":"+ seconds + " "+ timeZoneString);




		Log.i("DevInfo","exit RDI ");

	}

	public void onClick(View v){
//		Log.i("WebViewButtons","button clicked ");
		switch (v.getId()){


			case R.id.about:
//				minVideo();

				this.showFragment(
						newFrag2,
						R.id.video_container2);

				Log.i("WebViewButtons","about button clicked ");
				break;
			case R.id.cornerButton:
				Log.i("WebViewButtons","corner button clicked ");

				destroyItem(newFrag1);

				break;

			case R.id.check_in:
//				minVideo();


				this.showFragment(
						newFrag1,
						R.id.video_container1);
				Log.i("WebViewButtons","checkin button clicked ");
				break;

			case R.id.patient_survey:
				minVideo();
				Log.i("WebViewButtons","patient survey button clicked ");
				break;
			case R.id.patient_ed:
				minVideo();
				Log.i("WebViewButtons","patient ed button clicked ");
				break;
			case R.id.weather:
				minVideo();
				Log.i("WebViewButtons","weather button clicked ");
				break;
			case R.id.return_to_video:
				maxVideo();
				Log.i("WebViewButtons","returntovid button clicked ");
				break;


			case R.id.learn:
				minVideo();
				Log.i("WebViewButtons","learn button clicked ");
				break;

			case R.id.free:
				minVideo();
				Log.i("WebViewButtons","free button clicked ");
				break;

			case R.id.feedback:
				minVideo();
				Log.i("WebViewButtons","feedback button clicked ");
				break;

			case R.id.favorites:
				minVideo();
				Log.i("WebViewButtons","favorites button clicked ");
				break;


			case R.id.sky:
				minVideo();
				Log.i("WebViewButtons","sky button clicked ");
				break;

			case R.id.video_container1:

				Log.i("PIP","clicked vc1 ");
//				destroyItem(newFrag1);
//				minVideo();

				FrameLayout vidCont1;
				FrameLayout vidCont2;


				VideoFragment2 tempFrag2;


				String video = "rtp://@224.0.0.2:10000";
				tempFrag2 = VideoFragment2.newInstance(video);


				vidCont1 = (FrameLayout)findViewById(R.id.video_container1);
				vidCont2 = (FrameLayout)findViewById(R.id.video_container2);


				Log.i("PIP","pre destroy ");

//				destroyItem(newFrag1);

//				FragmentManager manager = ((Fragment) newFrag1).getFragmentManager();
//				FragmentTransaction trans = manager.beginTransaction();
//				trans.remove(newFrag1).commit();
				Log.i("PIP","post destroy ");

				newFrag2 = VideoFragment2.newInstance(video);


//				vidCont1.setVisibility(View.INVISIBLE);
				Log.i("PIP","pre showfrag ");

//
				this.showFragment(
						tempFrag2,
						R.id.video_container2);

				Log.i("PIP","post showfrag ");

				vidCont2.setVisibility(View.VISIBLE);
//
				Log.i("PIP","end vc1 click ");


				destroyItem(newFrag1);

				break;

			case R.id.video_container2:

//				maxVideo();

//				FrameLayout vidCont1;
//				FrameLayout vidCont2;

//				FrameLayout vidCont1;
//				FrameLayout vidCont2;
				video = "rtp://@224.0.0.2:10000";
				VideoFragment tempFrag1;

				tempFrag1 = VideoFragment.newInstance(video);

				vidCont2 = (FrameLayout)findViewById(R.id.video_container2);
				vidCont1 = (FrameLayout)findViewById(R.id.video_container1);

//
//				destroyItem(newFrag2);

//				manager = ((Fragment) newFrag2).getFragmentManager();
//				trans = manager.beginTransaction();
//				trans.remove(newFrag2).commit();


				newFrag1 = VideoFragment.newInstance("rtp://@224.0.0.2:10000");


//				vidCont2.setVisibility(View.INVISIBLE);


				this.showFragment(
						tempFrag1,
						R.id.video_container1);
//				this.showFragment(VideoInfoFragment.newInstance(""),
//						R.id.video_container);

				vidCont1.setVisibility(View.VISIBLE);



				break;
		}





//
	}

	public void minVideo(){
//		Log.i("WebViewButtons","vidCont1 button clicked ");

		FrameLayout vidCont1;
		FrameLayout vidCont2;

		vidCont1 = (FrameLayout)findViewById(R.id.video_container1);
		vidCont2 = (FrameLayout)findViewById(R.id.video_container2);

		String video = "rtp://@224.0.0.2:10000";

		newFrag2 = VideoFragment2.newInstance(video);


		vidCont1.setVisibility(View.INVISIBLE);

//
		this.showFragment(
				newFrag2,
				R.id.video_container2);
	}

	public void maxVideo(){
//		Log.i("WebViewButtons","vidCont2 button clicked ");

//				FrameLayout vidCont2;
		FrameLayout vidCont1;
		FrameLayout vidCont2;

		vidCont2 = (FrameLayout)findViewById(R.id.video_container2);
		vidCont1 = (FrameLayout)findViewById(R.id.video_container1);

//				String video = "rtp://@224.0.0.2:10000";

		newFrag1 = VideoFragment.newInstance("rtp://@224.0.0.2:10000");


		vidCont2.setVisibility(View.INVISIBLE);


		this.showFragment(
				newFrag1,
				R.id.video_container1);
//				this.showFragment(VideoInfoFragment.newInstance(""),
//						R.id.video_container);

		vidCont1.setVisibility(View.VISIBLE);
	}


	public class conTest2 extends AsyncTask<Void,Void,Void> {



		protected Void doInBackground(Void... params) {


			Log.i("php","comtest2 start"  );

			HttpURLConnection connection;
			OutputStreamWriter request = null;


			OutputStream os = null;
			InputStream is = null;

			URL url = null;
			String response = null;

			JSONObject jsonObject = new JSONObject();






			String parameters = "username="+"mUsername"+"&password="+"mPassword";

			try
			{

				jsonObject.put("NEWname", "NEWtestName");
				jsonObject.put("NEWdescription", "NEWtestUsername");
				jsonObject.put("NEWprice", "NEWmypassword");
				jsonObject.put("NEWage", "NEW23");

				String message = jsonObject.toString();

				url = new URL("http://myconstantcare.com/android_connect/android_post_test.php");

				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				connection.setRequestMethod("POST");



				connection.setReadTimeout( 10000 /*milliseconds*/ );
				connection.setConnectTimeout( 15000 /* milliseconds */ );
//				connection.setRequestMethod("POST");
				connection.setDoInput(true);
//				connection.setDoOutput(true);
				connection.setFixedLengthStreamingMode(message.getBytes().length);

				//make some HTTP header nicety
				connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
				connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
				connection.setRequestProperty("Content-Type", "application/json");

				connection.connect();


				request = new OutputStreamWriter(connection.getOutputStream());
//				request.write(parameters);

				os = new BufferedOutputStream(connection.getOutputStream());

				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));


//				os.write(message.getBytes());
				writer.write(message.toString());
				writer.close();
				os.close();
//				os.flush();
				is = connection.getInputStream();
				Log.i("php","is = " +is.toString() );
				Log.i("php","message = " +message );



				request.flush();
				request.close();
				String line = "";
				InputStreamReader isr = new InputStreamReader(connection.getInputStream());
				BufferedReader reader = new BufferedReader(isr);
				StringBuilder sb = new StringBuilder();
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				// Response from server after login process will be stored in response variable.
				response = sb.toString();
				// You can perform UI operations here
//            Toast.makeText(this,"Message from Server: \n"+ response, 0).show();
				isr.close();
				reader.close();

				Log.i("php","comtest2 results = " + response );


			}
			catch(IOException e)
			{
				// Error
				Log.i("php","catch" + e );

			}
			catch(JSONException j){

			}


			return null;
		}

	}



	public class conTest3 extends AsyncTask<Void,Void,Void> {



		protected Void doInBackground(Void... params) {


			Log.i("php","comtest2 start"  );

			HttpURLConnection connection;
			OutputStreamWriter request = null;


			OutputStream os = null;
			InputStream is = null;

			URL url = null;
			String response = null;








			String parameters = "username="+"mUsername"+"&password="+"mPassword";

			try
			{




				url = new URL("http://myconstantcare.com/android_connect/android_post_test.php");

				connection = (HttpURLConnection) url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				connection.setRequestMethod("POST");


				request = new OutputStreamWriter(connection.getOutputStream());
				request.write(parameters);



				request.flush();
				request.close();
				String line = "";
				InputStreamReader isr = new InputStreamReader(connection.getInputStream());
				BufferedReader reader = new BufferedReader(isr);
				StringBuilder sb = new StringBuilder();
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				// Response from server after login process will be stored in response variable.
				response = sb.toString();
				// You can perform UI operations here
//            Toast.makeText(this,"Message from Server: \n"+ response, 0).show();
				isr.close();
				reader.close();

				Log.i("php","comtest2 results = " + response );


			}
			catch(IOException e)
			{
				// Error
				Log.i("php","catch" + e );

			}



			return null;
		}

	}



}
