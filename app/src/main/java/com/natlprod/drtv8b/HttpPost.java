package com.natlprod.drtv8b;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by john on 7/15/2016.
 */
public class HttpPost extends Activity {

    TextView content;
    EditText fname, email, login, pass;
    String Name, Email, Login, Pass;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_http_post_example);

//        content    =   (TextView)findViewById( R.id.content );
//        fname      =   (EditText)findViewById(R.id.name);
//        email      =   (EditText)findViewById(R.id.email);
//        login      =    (EditText)findViewById(R.id.loginname);
//        pass       =   (EditText)findViewById(R.id.password);


//        Button saveme=(Button)findViewById(R.id.save);

//        saveme.setOnClickListener(new Button.OnClickListener(){
    }
    public void onClick(View v) {
        try {

            // CALL GetText method to make post method call
            GetText();
        } catch (Exception ex) {
            content.setText(" url exeption! ");
        }
    }


    // Create GetText Metod
    public void GetText() throws UnsupportedEncodingException {


        OutputStream os = null;
        InputStream is = null;
        HttpURLConnection conn = null;
        try {
            //constants
            URL url = new URL("http://myconstantcare.com/android_connect/get_all_members.php");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("NEWname", "NEWtestName");
            jsonObject.put("NEWdescription", "NEWtestUsername");
            jsonObject.put("NEWprice", "NEWmypassword");
            jsonObject.put("NEWage", "NEW23");

            String message = jsonObject.toString();

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);

            //make some HTTP header nicety
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            //open
            conn.connect();

            //setup send
            os = new BufferedOutputStream(conn.getOutputStream());
            os.write(message.getBytes());
            //clean up
            os.flush();

            //do somehting with response
            is = conn.getInputStream();

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);
            JSONObject returnJson = new JSONObject(responseStrBuilder.toString());


            Log.i("php", returnJson.toString());




//            String contentAsString = readIt(is,len);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            //clean up
            try {
                os.close();
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            conn.disconnect();
        }




//        // Get user defined values
//        Name = fname.getText().toString();
//        Email = email.getText().toString();
//        Login = login.getText().toString();
//        Pass = pass.getText().toString();
//
//        // Create data variable for sent values to server
//
//        String data = "data";
//        ArrayList<ArrayList> dataToSend = new ArrayList<>();
//
//        ArrayList<String> dataSet1 = new ArrayList<>();
//        dataSet1.add("NEWname");
//        dataSet1.add("NEWtestName");
//
//        ArrayList<String> dataSet2 = new ArrayList<>();
//        dataSet1.add("NEWprice");
//        dataSet1.add("NEWtestUsername");
//
//        ArrayList<String> dataSet3 = new ArrayList<>();
//        dataSet1.add("NEWprice");
//        dataSet1.add("NEWmypassword");
//
//        ArrayList<String> dataSet4 = new ArrayList<>();
//        dataSet1.add("NEWprice");
//        dataSet1.add("NEW23");
//
//        dataToSend.add(dataSet1);
//        dataToSend.add(dataSet2);
//        dataToSend.add(dataSet3);
//        dataToSend.add(dataSet4);
//
//
//
////                URLEncoder.encode("name", "UTF-8")
////                + "=" + URLEncoder.encode(Name, "UTF-8");
////
////        data += "&" + URLEncoder.encode("email", "UTF-8") + "="
////                + URLEncoder.encode(Email, "UTF-8");
////
////        data += "&" + URLEncoder.encode("user", "UTF-8")
////                + "=" + URLEncoder.encode(Login, "UTF-8");
////
////        data += "&" + URLEncoder.encode("pass", "UTF-8")
////                + "=" + URLEncoder.encode(Pass, "UTF-8");
//
//        String text = "";
//        BufferedReader reader = null;
//
//        // Send data
//        try {
//
//            // Defined URL  where to send data
////            URL url = new URL("http://androidexample.com/media/webservice/httppost.php");
//            URL url = new URL("http://myconstantcare.com/android_connect/get_all_members.php");
//
//            // Send POST data request
//
//            URLConnection conn = url.openConnection();
//            conn.setDoOutput(true);
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//            wr.write(data);
//            wr.write(dataToSend);
//
//            wr.flush();
//
//            // Get the server response
//
//            reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            StringBuilder sb = new StringBuilder();
//            String line = null;
//
//            // Read Server Response
//            while ((line = reader.readLine()) != null) {
//                // Append server response in string
//                sb.append(line + "\n");
//            }
//
//
//            text = sb.toString();
//        } catch (Exception ex) {
//
//        } finally {
//            try {
//
//                reader.close();
//            } catch (Exception ex) {
//            }
//        }
//
//        // Show response on activity
//        content.setText(text);

    }

}
