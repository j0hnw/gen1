package com.natlprod.drtv8b;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import android.os.StrictMode;
import android.util.Log;

public class AdSparx {
    public final static String TAG = "AdSparxActivity";
	
    public static String serverComm(final String url, Boolean isLine) {
    	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy);    		
    	try {
	    			URL lUrl = new URL(url);
	    			URI uri = new URI(lUrl.getProtocol(), lUrl.getUserInfo(),
	    					lUrl.getHost(), lUrl.getPort(), lUrl.getPath(),
	    					lUrl.getQuery(), lUrl.getRef());
	    			Log.i(TAG, lUrl.toString());
	    			
	    			HttpURLConnection con = (HttpURLConnection) lUrl.openConnection();
	    			  Log.e(TAG, "HTTP Request Code " + con.getResponseCode());
	    			  int statusCode = con.getResponseCode();
	    			  if (statusCode == 200) {
	    				  Log.e(TAG, "Request made successfully" + statusCode);
	    				  return readStream(con.getInputStream(), isLine);
	    			  }
	    		} catch (Exception ex) {
	    			ex.printStackTrace();
	    			return null;
	    		}
				return null;
	  		  
	 	}
  			
	public static String serverComm(String url) {
		return serverComm(url, false);
	}

	public float computeBandwith() {
		URL url;
		try {
			url = new URL("http://www.android.com/");
			long startTime = System.currentTimeMillis();
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			long endTime = System.currentTimeMillis();
			final long contentLength = con.getContentLength();
			float bandwidth = contentLength / ((endTime - startTime) * 1000);
			return bandwidth;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	static String readStream(InputStream in, Boolean isLine) {
		BufferedReader reader = null;
		StringBuilder stringBuilder = new StringBuilder();
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				if(isLine){
					stringBuilder.append(System.getProperty("line.separator"));
				}
			}
		} catch (IOException e) {
			Log.e(TAG, e.toString());
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Log.e(TAG, e.toString());
				}
			}
		}
		return stringBuilder.toString();
	}

	public static String reportingServer(String url, JSONObject report) {
		HttpResponse response = null;
		String response1 =null;
		try {
			String send = report.toString();
	        HttpPost httpPost = new HttpPost(url);
	        httpPost.setEntity(new StringEntity(send));
	        httpPost.setHeader("Accept", "application/json");
	        httpPost.setHeader("Content-type", "application/json");
	        response = new DefaultHttpClient().execute(httpPost);
	        Log.i(TAG, "httpPost response is "+response );
	        if(response.getStatusLine().getStatusCode() == 200){
	        	response1 = readStream(response.getEntity().getContent(), true);
	        }else {
	        	return response1;
	        }	
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		return response1;
	}
  
}