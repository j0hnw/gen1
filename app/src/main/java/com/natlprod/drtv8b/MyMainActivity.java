package com.natlprod.drtv8b;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class MyMainActivity extends AppCompatActivity {


    public String pageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);



        Intent intent = getIntent();
        int venueIndex = intent.getIntExtra("venueIndex", 0);
        String venueUrl = intent.getStringExtra("venueUrl");

        Log.i("spinner","venueIndex = " + venueIndex );



        WebView webview = (WebView) findViewById(R.id.webView);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheMaxSize(5 * 1024 * 1024);
        webSettings.setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);

        if (! isNetworkAvailable()){
            webview.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        }


        //autoplay ?
        //webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        //webview.clearCache(true);
        webview.clearHistory();
//        webview.loadUrl("http://myconstantcare.com/php_tests/drtv/demo.html");
        webview.loadUrl(venueUrl);

//        webview.loadUrl("http://myconstantcare.com/php_tests/drtv/sstest.html");


        new conTest().execute();
        Log.i("php","serverCom " );

       String phpResults =  serverComm("http://myconstantcare.com/android_connect/android_post_test.php",false);

        Log.i("php","phpResults" + phpResults );

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("somedomain.com") == true) {
                    view.loadUrl(url);
                }
                return true;


                //return false;
            }
        });

        webview.setWebChromeClient(new WebChromeClient());
// timer





        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                reportDeviceInformation();
            }
        }, 0, 6000);//put here time 1000 milliseconds=1 second


        reportDeviceInformation();

    }



    public static String connTest2 ()
    {

        Log.i("php","comtest2 start"  );

        HttpURLConnection connection;
        OutputStreamWriter request = null;

        URL url = null;
        String response = null;
        String parameters = "username="+"mUsername"+"&password="+"mPassword";

        try
        {
            url = new URL("http://myconstantcare.com/android_connect/android_post_test.php");
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");

            request = new OutputStreamWriter(connection.getOutputStream());
            request.write(parameters);
            request.flush();
            request.close();
            String line = "";
            InputStreamReader isr = new InputStreamReader(connection.getInputStream());
            BufferedReader reader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            // Response from server after login process will be stored in response variable.
            response = sb.toString();
            // You can perform UI operations here
//            Toast.makeText(this,"Message from Server: \n"+ response, 0).show();
            isr.close();
            reader.close();

            Log.i("php","comtest2 results = " + response );

            return response;
        }
        catch(IOException e)
        {
            // Error
            Log.i("php","catch" );

        }

        return null;
    }





//    public void conTest(){
    public class conTest extends AsyncTask<Void,Void,Void> {



    protected Void doInBackground(Void... params) {

        OutputStream os = null;
        InputStream is = null;
        HttpURLConnection conn = null;




        try {

//            Log.i("php","begin try");
//
//
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("pid", "NEWpid");
//
//            jsonObject.put("name", "NEWtestName");
//            jsonObject.put("price", "NEWmypassword");
//            jsonObject.put("description", "NEWtestUsername");
//
//            String message = jsonObject.toString();
//
//            URL url = new URL("http://myconstantcare.com/android_connect/update_product.php");
////            URL url = new URL("http://myconstantcare.com/android_connect/update_product.php");
//            Log.i("php","post set url");
//
//            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//            Log.i("php","post set urlConnection");
//
//            urlConnection.setDoOutput(true);
//            urlConnection.setChunkedStreamingMode(0);
//
//            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
//            out.write(message.getBytes());
////            writeStream(out);
//            Log.i("php","post out.write");
//
////            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
//            InputStream in = urlConnection.getInputStream();
//
//            Log.i("php","post in dec");
//
//            BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
//            Log.i("php","post streamReader dec");
//
//            StringBuilder responseStrBuilder = new StringBuilder();
//            Log.i("php","post responsestrBuild");
//
//            String inputStr;
//            Log.i("php","pre while");
//
//            while ((inputStr = streamReader.readLine()) != null)
//                responseStrBuilder.append(inputStr);
//            JSONObject returnJson = new JSONObject(responseStrBuilder.toString());
//
//            Log.i("php","json object"+ jsonObject);
//
//
//            Log.i("php","request"+ message);
//            Log.i("php","request bytes"+ message.getBytes());
//            Log.i("php","output string"+ os.toString());
//
//
//
//            Log.i("php","json return"+ returnJson.toString());
//            urlConnection.disconnect();
//            Log.i("php", "end of try");
//
//        }
//        catch (Exception e){
//            Log.i("php","exeption cought ... "+ e);
//
//
//        }
//        finally {
//
//        }

//***********************

//            constants
//            URL url = new URL("http://myconstantcare.com/android_connect/get_all_members.php");
//            URL url = new URL("http://myconstantcare.com/android_connect/update_product.php");
            URL url = new URL("http://myconstantcare.com/android_connect/android_post_test.php");
//            postData


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("pid", "NEWpid");

            jsonObject.put("name", "NEWtestName");
            jsonObject.put("price", "NEWmypassword");
            jsonObject.put("description", "NEWtestUsername");

//            jsonObject.put("age", "NEW23");

            String message = jsonObject.toString();

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /*milliseconds*/);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("POST");
//            conn.setRequestProperty();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setFixedLengthStreamingMode(message.getBytes().length);


            //make some HTTP header nicety
//            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");

            Log.i("php", "conn = " + conn);
            Log.i("php", "conn is = " + conn.getInputStream());
//            Log.i("php", "conn = " + conn);
//            Log.i("php", "conn = " + conn);


            //open
            conn.connect();

            //setup send
            os = new BufferedOutputStream(conn.getOutputStream());
//            os.write(message.getBytes());
            os.write(message.getBytes());

            //clean up
            os.flush();

            //do somehting with response
            is = conn.getInputStream();

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);
            JSONObject returnJson = new JSONObject(responseStrBuilder.toString());
//
//            Log.i("php","json object"+ jsonObject);
//
//
//            Log.i("php","request"+ message);
//            Log.i("php","request bytes"+ message.getBytes());
//            Log.i("php","output string"+ os.toString());
//
//
//
//            Log.i("php","json return"+ returnJson.toString());


            os.close();
            is.close();

            Log.i("php","json object"+ jsonObject);


            Log.i("php","request"+ message);
            Log.i("php","request bytes"+ message.getBytes());
            Log.i("php", "output string" + os.toString());



            Log.i("php", "json return" + returnJson.toString());
//            String contentAsString = readIt(is,len);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            //clean up
//            try {
////                os.close();
////                is.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

            conn.disconnect();
        }


//        **************


//        OutputStream os = null;
//        InputStream is = null;
//        HttpURLConnection conn = null;
//        try {
//            //constants
//            URL url = new URL("http://myconstantcare.com/android_connect/get_all_members.php");
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.put("NEWname", "NEWtestName");
//            jsonObject.put("NEWdescription", "NEWtestUsername");
//            jsonObject.put("NEWprice", "NEWmypassword");
//            jsonObject.put("NEWage", "NEW23");
//
//            String message = jsonObject.toString();
//
//            conn = (HttpURLConnection) url.openConnection();
//            conn.setReadTimeout( 10000 /*milliseconds*/ );
//            conn.setConnectTimeout( 15000 /* milliseconds */ );
//            conn.setRequestMethod("POST");
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//            conn.setFixedLengthStreamingMode(message.getBytes().length);
//
//            //make some HTTP header nicety
//            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
//            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
//
//            //open
//            conn.connect();
//
//            //setup send
//            os = new BufferedOutputStream(conn.getOutputStream());
//            os.write(message.getBytes());
//            //clean up
//            os.flush();
//
//            //do somehting with response
//            is = conn.getInputStream();
//
//            //String contentAsString = readIt(is,len);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (JSONException e) {
//            e.printStackTrace();
//        } finally {
//            //clean up
//            try {
//                os.close();
//                is.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            conn.disconnect();
//        }
        return null;
    }
    }

    private void reportDeviceInformation(){
        Log.i("DevInfo","enter RDI ");

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = this.registerReceiver(null, ifilter);


        // Are we charging / charged?
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;

// How are we charging?
        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;


// battery level
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float)scale;

//log changing status
        Log.i("DevInfo","Charging = "+isCharging);
        Log.i("DevInfo","usbCharge = "+usbCharge);
        Log.i("DevInfo","acCharge = "+acCharge);
        Log.i("DevInfo","batteryPct = "+batteryPct);




        Log.i("DevInfo","exit RDI ");

    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService( CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public static String serverComm(final String url, Boolean isLine) {
        String TAG = "php";
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL lUrl = new URL(url);
            URI uri = new URI(lUrl.getProtocol(), lUrl.getUserInfo(),
                    lUrl.getHost(), lUrl.getPort(), lUrl.getPath(),
                    lUrl.getQuery(), lUrl.getRef());
            Log.i(TAG,"url string " + lUrl.toString());

            HttpURLConnection con = (HttpURLConnection) lUrl.openConnection();
            Log.e(TAG, "HTTP Request Code " + con.getResponseCode());
            int statusCode = con.getResponseCode();
            if (statusCode == 200) {
                Log.e(TAG, "Request made successfully" + statusCode);
                return readStream(con.getInputStream(), isLine);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return null;

    }



    static String readStream(InputStream in, Boolean isLine) {
        String TAG = "php";

        BufferedReader reader = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                if(isLine){
                    stringBuilder.append(System.getProperty("line.separator"));
                }
            }
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                }
            }
        }
        return stringBuilder.toString();
    }
}
