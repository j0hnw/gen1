package com.natlprod.drtv8b;

import org.json.JSONException;
import org.json.JSONObject;

//import com.compdigitec.libvlcandroidsample.R;

//import Adsparxutils.AdSparx;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.Toast;

public class OrgMain extends Activity  {
    public final static String TAG = "VideoActivity";
    //Adsparx components.
    String deviceId;
    String plotterId;
    int nextReq;

    public final static String LOCATION = "com.compdigitec.libvlcandroidsample.VideoActivity.location";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.layout);

        String video = "rtp://@224.0.0.2:10000";

        this.showFragment(
                VideoFragment.newInstance(video),
                R.id.video_container1);
        this.showFragment(VideoInfoFragment.newInstance(""),
                R.id.video_container);
        startAdsparx();
        /*Configuration currentConfiguration = getResources().getConfiguration();
        if(currentConfiguration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
        	VideoInfoFragment(View.GONE);
        }*/

    }


    protected void showFragment(Fragment newFragment, int container) {
        FragmentTransaction transaction = this.getFragmentManager()
                .beginTransaction();
        transaction.add(container, newFragment);
        transaction.commit();
    }
    /*
     * Hide Video elements if you want
     * (non-Javadoc)
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            VideoInfoFragment(View.VISIBLE);

        } else {
            VideoInfoFragment(View.INVISIBLE);

        }
    }



    private void VideoInfoFragment(int visible) {
        // TODO Auto-generated method stub
        Log.d(TAG, "removing the second fragmet");
        FrameLayout listFragmentContainer = (FrameLayout) findViewById(R.id.video_container);
        listFragmentContainer.setVisibility(visible);

    }


    @Override
    public void onResume(){
        super.onResume();

        Log.d(TAG, "onResume");
    }


    @Override
    public void onPause(){
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }


    public void startAdsparx() {
        WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        deviceId = info.getMacAddress();
        deviceId = deviceId.replace(":", "-");
        Log.i(TAG, "device id"+deviceId);
        String plotterID = getPlotterID(deviceId);
        if (plotterID != null) {
            FrameLayout listFragmentContainer = (FrameLayout) findViewById(R.id.video_container1);
            listFragmentContainer.setOnTouchListener(new OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    Log.i(TAG, "Adsparx OnTouch:" + event.toString());
                    if (event.getAction() != MotionEvent.ACTION_DOWN) {
                        return false;
                    }
                    return doClick();
                }
            });
            nextReq = 0;
            reportServer(deviceId, plotterID, "view",nextReq);
        }else {
            Toast.makeText(getBaseContext(), "connection to plotter failed.",
                    Toast.LENGTH_LONG).show();
        }
    }



    private boolean doClick() {

        String urlToOpen = hitMetaURl();
        if(urlToOpen != null){
            hitClickReport();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(urlToOpen));
            startActivity(i);
        }
        return true;
    }


    private void hitClickReport() {

        reportServer(deviceId, plotterId, "click", 0);

    }

    private String hitMetaURl() {

        String metaURL = null;
        String metaReqUrl = "http://192.168.10.106:8080/getmetaURL/"+deviceId+"/"+plotterId;
        String metaURLResp = AdSparx.serverComm(metaReqUrl,true);
        if (metaURLResp !=null){
            try {
                metaURL = metaURLResp.replaceAll("\\\\", "");
                JSONObject url = new JSONObject(metaURL);
                metaURL = url.getString("metaUrl");
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            Toast.makeText(getBaseContext(), "Error occured during MetaURL..!!",
                    Toast.LENGTH_LONG).show();
        }
        return metaURL;

    }
    private String getPlotterID(String deviceId) {

        String url = "http://cms.gen1mediagroup.com/manage/api/getPlotterId/"+deviceId+"/";
        //String url = "http://107.170.71.238/manage/api/getPlotterId/12-34-56-78-90/";
        String resp = AdSparx.serverComm(url, true);

        Log.i(TAG, "Res URL: " + resp);

        try {
            String myJsonString = resp.replaceAll("\\\\","");
            JSONObject job = new JSONObject(myJsonString);
            plotterId =job.getJSONObject("message").getString("PlotterID");
            Log.i(TAG, "mediaID: " + plotterId);

        } catch(JSONException e) {
            Log.e(TAG, e.toString());
        }
        return plotterId;

    }
	/*
	 * {"watchedVideoFileName":"","watchedVideoProgress":0,"plotterId":"AVDSFJKJHdsjh4372", "deviceId":"dc-74-fd-43-sd-34-54","deviceUA":"","deviceIP":"","event":"view"}
	 * */

    private void reportServer(final String deviceId2, final String plotterID,final String event,final int time) {
        final String reportingserver = "http://192.168.10.106:8080/deviceinfo";
        String userAgent = System.getProperty("http.agent");
        final JSONObject report = new JSONObject();
        try {
            report.put("watchedVideoFileName", "");
            report.put("watchedVideoProgress", "");
            report.put("plotterId", plotterID);
            report.put("macId", deviceId2);
            report.put("deviceUA",userAgent );
            report.put("deviceIP", "");
            report.put("event", event);
            Log.i(TAG, "Res URL: " + report);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (event == "click"){
            AdSparx.reportingServer(reportingserver ,report);
        }else {
            eventReport(reportingserver,report,time);
        }

    }

    private void eventReport(final String reportingserver, final JSONObject report, final int time) {
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Log.i("tag", "This'll run "+time+"milliseconds later");
                        String nextReqToHit = AdSparx.reportingServer(reportingserver ,report);
                        if (nextReqToHit !=null ){
                            try {
                                JSONObject hitInterval = new JSONObject(nextReqToHit);
                                nextReq = hitInterval.getInt("hitAfter");
                                Log.i(TAG, "What is the next request interval"+nextReq);
                                if (nextReq >0){
                                    eventReport(reportingserver, report, nextReq);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else {
                            Toast.makeText(getBaseContext(), "reporting failed",
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                },
                time);

    }

}
