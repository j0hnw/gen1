package com.natlprod.drtv8b;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/*
 * 
 */
public class VideoInfoFragment extends Fragment {

	public static VideoInfoFragment newInstance(String videoURL) {
		VideoInfoFragment fragment = new VideoInfoFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        return view;
    }
}