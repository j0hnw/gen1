package com.natlprod.drtv8b;
//package com.natlprod.drtv8;


import android.app.Fragment;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import org.videolan.libvlc.EventHandler;
import org.videolan.libvlc.IVideoPlayer;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaList;

import java.lang.ref.WeakReference;


public class VideoFragment2 extends Fragment implements SurfaceHolder.Callback, IVideoPlayer{
	public final static String TAG = "LibVLCAndroidSample/VideoActivity";

    public final static String LOCATION = "com.compdigitec.libvlcandroidsample.VideoActivity.location";
    public static final String VIDEO_URL = "VIDEO_URL";
    private String mFilePath;
    
    
    
    // display surface
    private static SurfaceView mSurface;
    private SurfaceHolder holder;

    // media player
    private LibVLC libvlc;
    private int mVideoWidth;
    private int mVideoHeight;
  	private final static int VideoSizeChanged = -1;
	private static Context mContext;
	private EventHandler mEventHandler;
	private ViewGroup mContainer;
	
    public static VideoFragment2 newInstance(String videoURL) {
		VideoFragment2 fragment = new VideoFragment2();
		Bundle args = new Bundle();
		args.putString(VIDEO_URL, videoURL);

		fragment.setArguments(args);

	//	fragment.setSize(301,170);
		return fragment;
	}
    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {	
		Log.d(TAG, "onCreateView");
		
		View view = inflater.inflate(R.layout.fragment_video, container, false);

		mContainer = container;
		mContext = getActivity();
		mFilePath =  this.getArguments().getString(VIDEO_URL);
		mSurface = (SurfaceView) view.findViewById(R.id.fragment_videoview_surface);
		holder = mSurface.getHolder();
		holder.addCallback(this);
		 
		return view;
	}
	
    public void createPlayer() {
		releasePlayer();
		try {
			String media = mFilePath;
			if (media.length() > 0) {
               			Toast toast = Toast.makeText(mContext, media, Toast.LENGTH_LONG);
                		toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0,
                	        0);
               		 toast.show();
            		}

			
			// Create a new media player
			
			libvlc = LibVLC.getInstance();
			libvlc.setHardwareAcceleration(LibVLC.HW_ACCELERATION_DISABLED);
			libvlc.setSubtitlesEncoding("");
			libvlc.setAout(LibVLC.AOUT_OPENSLES);
			libvlc.setTimeStretching(true);
			libvlc.setChroma("RV32");
			libvlc.setVerboseMode(true);
			LibVLC.restart(mContext);
			EventHandler.getInstance().addHandler(mHandler);
			holder.setFormat(PixelFormat.RGBX_8888);
            holder.setKeepScreenOn(true);
			MediaList list = libvlc.getMediaList();
			list.clear();
			list.add(new Media(libvlc, LibVLC.PathToURI(media)), false);
			libvlc.setVolume(10);
			libvlc.playIndex(0);
					} catch (Exception e) {
			Log.e(TAG, e.toString());
			Toast.makeText(mContext, "Error creating player!",
					Toast.LENGTH_LONG).show();
		}
	}



	@Override
	public void setSurfaceSize(int width, int height, int visible_width, int visible_height, int sar_num, int sar_den) {
		Message msg = Message.obtain(mHandler, VideoSizeChanged, width, height);
        msg.sendToTarget();
	}
	
	public void releasePlayer() {
		if (libvlc == null)
			return;
		EventHandler.getInstance().removeHandler(mHandler);
		libvlc.stop();
		libvlc.detachSurface();
		libvlc.destroy();
		libvlc = null;

		mVideoWidth = 0;
		mVideoHeight = 0;
	}

	/*************
	 * Events
	 *************/

	private Handler mHandler = new MyHandler(this);

	private static class MyHandler extends Handler {
		private WeakReference<VideoFragment2> mOwner;

		public MyHandler(VideoFragment2 owner) {
			mOwner = new WeakReference<VideoFragment2>(owner);
		}

		@Override
		public void handleMessage(Message msg) {
			VideoFragment2 player = mOwner.get();

			// SamplePlayer events
			if (msg.what == VideoSizeChanged) {
				player.setSize(msg.arg1, msg.arg2);
				return;
			}

			// Libvlc events
			Bundle b = msg.getData();
			switch (b.getInt("event")) {
			case EventHandler.MediaPlayerEndReached:
				player.releasePlayer();
				break;
			case EventHandler.MediaPlayerPlaying:
				//player.startAdsparx();	
				//AdsparxManger.startAdSparx(mContext,mSurface);
			case EventHandler.MediaPlayerPaused:
			case EventHandler.MediaPlayerStopped:
			default:
				break;
			}
		}
	}
	
	private void setSize(int width, int height) {
		mVideoWidth = width;
        mVideoHeight = height;
        if (mVideoWidth * mVideoHeight <= 1)
            return;

        // get screen size
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int w = metrics.widthPixels;
        int h = metrics.heightPixels;

        // getWindow().getDecorView() doesn't always take orientation into
        // account, we have to correct the values
        boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        if (w > h && isPortrait || w < h && !isPortrait) {
            int i = w;
            w = h;
            h = i;
        }

        float videoAR = (float) mVideoWidth / (float) mVideoHeight;
        float screenAR = (float) w / (float) h;

        if (screenAR < videoAR)
            h = (int) (w / videoAR);
        else
            w = (int) (h * videoAR);

        // force surface buffer size
        holder.setFixedSize(mVideoWidth, mVideoHeight);

        // set display size
        LayoutParams lp = mSurface.getLayoutParams();
        lp.width = mContainer.getWidth();
        lp.height = mContainer.getHeight();
        mSurface.setLayoutParams(lp);
        mSurface.invalidate();
	}


	@Override
	public void surfaceCreated(SurfaceHolder holder) {
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if (libvlc != null)
            libvlc.attachSurface(holder.getSurface(), this);
		
	}
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
		holder = mSurface.getHolder();
		createPlayer();
	}
	
    @Override
    public void onPause() {
    	Log.d(TAG, "onPause");
        super.onPause();
        releasePlayer();
    }

    @Override
    public void onDestroy() {
    	Log.d(TAG, "onDestroy");
        super.onDestroy();
        releasePlayer();
    }

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
	}
	
	
}